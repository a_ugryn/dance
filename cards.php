<!DOCTYPE html>
<html lang="en">
  <?php include "head.php"; ?>
  <body>
  	<?php include "header.php"; ?>
    <!-- EVENTS -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="main-h1">Клубные карты</h2>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb text-center">
                    <li><a href="#">Главная</a></li>
                    <li class="active">Клубные карты</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="divider"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card-text">
                    <p>Приобретая карту,  вы становитесь членом клуба Школы танцев Александра Полякова DANCE SCHOOL. Членство дает право:<br>
                    - неограниченно посещать групповые занятия  (кроме владельцев карт Сlub card)<br>
                    - приглашать неограниченное количество гостей на регулярные мероприятия школы (кроме особых мероприятий).</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="filter clearfix">
                    <h3 class="filter-title">Найдите нужную карту!</h3>
                    <div class="clearfix">
                        <select id="date-card">
                            <option value="">Все карты</option>
                            <option value="1">1 год</option>
                            <option value="2">2 года</option>
                            <option value="3">3 года</option>
                            <option value="4">4 года</option>
                        </select>
                        <div class="filter-input">
                            <input id="filer-min-price" type="text" placeholder="Цена от..." />
                            <input id="filer-max-price" type="text" placeholder="Цена до..." />
                        </div>
                        <select id="type-card" >
                            <option value="">Тип карты</option>
                            <option value="">Тип 1</option>
                            <option value="">Тип 2</option>
                            <option value="">Тип 3</option>
                            <option value="">Тип 4</option>
                        </select>
                    </div>
                </div>
                <div class="filter-content">
                    <!-- single -->
                    <div class="filter-content-single clearfix" data-price="10000" data-date="1" data-typee="1">
                        <div class="img-card">
                            <img src="img/card/card-1.jpg" alt="" />
                        </div>
                        <div class="filter-content-single-mid">
                            <div class="card-title">Сlub card (1 год)</div>
                            <div class="card-description">
                                    Владельцу карты «Сlub card»предоставляется право на:<br>
                                    - неограниченное приобретение индивидуальных уроков с преподавателями любого уровня по цене, 
                                    действующей для членов клуба школы танцев. 
                            </div>
                            <div class="card-bot-text">
                                *карта действительна 12 месяцев с момента приобретения.
                            </div>
                        </div>
                        <div class="filter-content-single-right">
                            <div class="filter-content-single-right-price">
                                10000<span>руб.</span>    
                            </div>
                            <span class="text-center popup-button-margin">
                                <div class="button">
                                    <div class="button-border">
                                        <button class="button-inner card-but" data-card="Сlub card (1 год)">Заказать карту</button>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>

                     <!-- single -->
                    <div class="filter-content-single clearfix" data-price="500" data-date="2" data-typee="2">
                        <div class="img-card">
                            <img src="img/card/card-1.jpg" alt="" />
                        </div>
                        <div class="filter-content-single-mid">
                            <div class="card-title">Сlub card (2 годa)</div>
                            <div class="card-description">
                                    Владельцу карты «Сlub card»предоставляется право на:<br>
                                    - неограниченное приобретение индивидуальных уроков с преподавателями любого уровня по цене, 
                                    действующей для членов клуба школы танцев. 
                            </div>
                            <div class="card-bot-text">
                                *карта действительна 12 месяцев с момента приобретения.
                            </div>
                        </div>
                        <div class="filter-content-single-right">
                            <div class="filter-content-single-right-price">
                                500<span>руб.</span>    
                            </div>
                            <span class="text-center popup-button-margin">
                                <div class="button">
                                    <div class="button-border">
                                        <button class="button-inner call-to-less-button">Заказать карту</button>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>

                     <!-- single -->
                    <div class="filter-content-single clearfix" data-price="1500" data-date="3" data-typee="3">
                        <div class="img-card">
                            <img src="img/card/card-1.jpg" alt="" />
                        </div>
                        <div class="filter-content-single-mid">
                            <div class="card-title">Сlub card (3 годa)</div>
                            <div class="card-description">
                                    Владельцу карты «Сlub card»предоставляется право на:<br>
                                    - неограниченное приобретение индивидуальных уроков с преподавателями любого уровня по цене, 
                                    действующей для членов клуба школы танцев. 
                            </div>
                            <div class="card-bot-text">
                                *карта действительна 12 месяцев с момента приобретения.
                            </div>
                        </div>
                        <div class="filter-content-single-right">
                            <div class="filter-content-single-right-price">
                                1500<span>руб.</span>    
                            </div>
                            <span class="text-center popup-button-margin">
                                <div class="button">
                                    <div class="button-border">
                                        <button class="button-inner call-to-less-button">Заказать карту</button>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>

                     <!-- single -->
                    <div class="filter-content-single clearfix" data-price="5000" data-date="4" data-typee="4">
                        <div class="img-card">
                            <img src="img/card/card-1.jpg" alt="" />
                        </div>
                        <div class="filter-content-single-mid">
                            <div class="card-title">Сlub card (4 годa)</div>
                            <div class="card-description">
                                    Владельцу карты «Сlub card»предоставляется право на:<br>
                                    - неограниченное приобретение индивидуальных уроков с преподавателями любого уровня по цене, 
                                    действующей для членов клуба школы танцев. 
                            </div>
                            <div class="card-bot-text">
                                *карта действительна 12 месяцев с момента приобретения.
                            </div>
                        </div>
                        <div class="filter-content-single-right">
                            <div class="filter-content-single-right-price">
                                5000<span>руб.</span>    
                            </div>
                            <span class="text-center popup-button-margin">
                                <div class="button">
                                    <div class="button-border">
                                        <button class="button-inner call-to-less-button">Заказать карту</button>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="single-propose">
                    <h3>УСЛУГА «ИНДИВИДУАЛЬНЫЙ УРОК»</h3>
                    <p>В нашей школе Вы можете воспользоваться услугой индивидуального обучения (индивидуальный урок). <br>Индивидуальные уроки с преподавателем по любому из направлений. <br>Стоимость уточняйте по тел.: (863) 2 800-810, (928) 22 66 77 2</p>
                    <span>Специальное предложение</span>
                    <p>При покупке блока из 10 индивидуальный уроков - 11-й урок в подарок.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="divider-one"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="single-propose">
                    <h3>УСЛУГА «МИНИ ГРУППА»*</h3>
                    <p>Количество занимающихся от 3 до 5 человек <br>Стоимость для каждого участника «мини группы», составляет от 600руб (зависит от уровня преподавателя) <br>Эта форма обучения создана для желающих создать своё индивидуальное время для групповых занятий.</p>
                    <div>*проводится по предварительной записи и согласованию с преподавателем. <br>*только для держателей карт</div>
                </div>
            </div>
        </div>
    </div>




    <?php include "footer.php"; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/flexmenu.min.js"></script>
    <script src="js/jquery.plugin.js"></script>
    <script src="js/jquery.countdown.js"></script>
    <script src="js/parallax.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>