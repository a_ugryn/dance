<!DOCTYPE html>
<html lang="en">
  <?php include "head.php"; ?>
  <body>
    <?php include "header.php"; ?>
    <!-- EVENTS -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="main-h1">наши контакты</h2>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb text-center">
                    <li><a href="#">Главная</a></li>
                    <li class="active">Контакты</li>
                </ol>
            </div>
        </div>
    </div>
    
    <div class="map-container visible-lg visible-md">
        <div id="map" style="width: 100%; height: 670px"></div>
        <div class="contact-info">
            <div class="contact-info-inner">
                <div class="contact-info-inner-area">
                    <div>
                        <h3>КОНТАКТНАЯ ИНФОРМАЦИЯ</h3>
                        <img src="img/dev.png" alt="" />
                        <p>Телефоны: (863) 2 800 810, (928) 22 66 77 2 <br>E-Mail:  info@dsrnd.pro</p>
                    </div>
                    <div>
                        <h3>ЧАСЫ РАБОТЫ</h3>
                        <img src="img/dev.png" alt="" />
                        <p>10:00 — 23:00 Ежедневно без выходных</p>
                    </div>
                    <div>
                        <h3>адрес</h3>
                        <img src="img/dev.png" alt="" />
                        <p>Город: г. Ростов-на-Дону <br>Улица: ул.Максима Горького 11/43, вход с ул. М. Горького</p>
                    </div>
                    <span class="text-center popup-button-margin">
                        <div class="button">
                            <div class="button-border">
                                <button class="button-inner">Связаться с нами</button>
                            </div>
                        </div>
                    </span>
                </div>
            </div>
        </div>   
    </div>
    <div class="map-container visible-sm visible-xs">
        <div class="contact-info small-map">
            <div class="contact-info-inner">
                <div class="contact-info-inner-area">
                    <div>
                        <h3>КОНТАКТНАЯ ИНФОРМАЦИЯ</h3>
                        <img src="img/dev.png" alt="" />
                        <p>Телефоны: (863) 2 800 810, (928) 22 66 77 2 <br>E-Mail:  info@dsrnd.pro</p>
                    </div>
                    <div>
                        <h3>ЧАСЫ РАБОТЫ</h3>
                        <img src="img/dev.png" alt="" />
                        <p>10:00 — 23:00 Ежедневно без выходных</p>
                    </div>
                    <div>
                        <h3>адрес</h3>
                        <img src="img/dev.png" alt="" />
                        <p>Город: г. Ростов-на-Дону <br>Улица: ул.Максима Горького 11/43, вход с ул. М. Горького</p>
                    </div>
                    <span class="text-center popup-button-margin">
                        <div class="button">
                            <div class="button-border">
                                <button class="button-inner">Связаться с нами</button>
                            </div>
                        </div>
                    </span>
                </div>
            </div>
        </div>
        <div id="map2" style="width: 100%; height: 400px"></div>   
    </div>

    <?php include "footer.php"; ?>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/script.js"></script>
    <script type="text/javascript">
        ymaps.ready(init);
        var myMap, 
            myPlacemark;

        function init(){ 
            myMap = new ymaps.Map("map", {
                center: [47.22305129828324,39.699049499999994],
                zoom: 17
            });

            myMap2 = new ymaps.Map("map2", {
                center: [47.2229162982819,39.69589650000002],
                zoom: 17
            }); 
            
            myPlacemark = new ymaps.Placemark([47.2229162982819,39.69589650000002], {
                hintContent: 'Москва!',
                balloonContent: 'Столица России'
                },
            {
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                iconImageHref: 'img/map-pin.png',
                // Размеры метки.
                iconImageSize: [56, 77],
                iconImageOffset: [-25, -70]
            });

            myPlacemark2 = new ymaps.Placemark([47.2229162982819,39.69589650000002], {
                hintContent: 'Москва!',
                balloonContent: 'Столица России'
                },
            {
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                iconImageHref: 'img/map-pin.png',
                // Размеры метки.
                iconImageSize: [56, 77],
                iconImageOffset: [-25, -70]
            });
            
            myMap.geoObjects.add(myPlacemark);
            myMap2.geoObjects.add(myPlacemark2);
        }
    </script>
  </body>
</html>