<!DOCTYPE html>
<html lang="en">
  <?php include "head.php"; ?>
  <body>
  	<?php include "header.php"; ?>
    <!-- EVENTS -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="main-h1">спецпредложения</h2>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb text-center">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Направления</a></li>
                    <li><a href="#">Бальные танцы латиноамериканская программа</a></li>
                    <li class="active">Пасадобль</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="container container-padding">
        <div class="row">
            <div class="col-lg-12">
                <div class="special clearfix">
                    <img class="special-img" src="img/data/dance.jpg" alt="" />
                    <p>Пасодобль - танец имитирующий сражение матадора с быком. Партнер в танце изображает тореро, а партнерша- его плащ(мулето), иногда- второго быка. Движения в Пасодобле исполнены гордости, надменности и превосходства над силами природы, которые олицетворяет бык. Особенностью этого танца является позиция корпуса- грудь высоко поднята, плечи развернуты и опущены. Все движения соответствуют стилю матадора на испанской корриде, вес тела сконцентрирован впереди, почти все шаги делаются с каблука. Музыка в Пасодобле торжественна, мажорна, претенциозна, что объясняет маршевый характер танца. Танцоры исполняют 3 музыкальные части, каждая из которых соответствует: началу корриды, бой быка с тореодором, а третья- торжество победы.</p>
                    <p>Характер: торжественные, точные движения.</p>
                    <p>Муз размер: 2/4</p>
                    <p>Кол-во тактов в мин: 60-62</p>
                </div>
                <div class="text-center special-margin clearfix">
                    <div class="button">
                        <div class="button-border">
                            <button class="button-inner">Бесплатный урок</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="divider-one"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="dance-gallery clearfix">
                    <h3>фотографии танца</h3>
                    <div class="clearfix text-center">
                        <a href="img/data/dance-gall.jpg" data-lightbox="roadtrip"><img src="img/data/dance-gall.jpg" alt=""></a>
                        <a href="img/data/dance-gall.jpg" data-lightbox="roadtrip"><img src="img/data/dance-gall.jpg" alt=""></a>
                        <a href="img/data/dance-gall.jpg" data-lightbox="roadtrip"><img src="img/data/dance-gall.jpg" alt=""></a>
                        <a href="img/data/dance-gall.jpg" data-lightbox="roadtrip"><img src="img/data/dance-gall.jpg" alt=""></a>
                        <a href="img/data/dance-gall.jpg" data-lightbox="roadtrip"><img src="img/data/dance-gall.jpg" alt=""></a>
                        <a href="img/data/dance-gall.jpg" data-lightbox="roadtrip"><img src="img/data/dance-gall.jpg" alt=""></a>
                        <a href="img/data/dance-gall.jpg" data-lightbox="roadtrip"><img src="img/data/dance-gall.jpg" alt=""></a>
                        <a href="img/data/dance-gall.jpg" data-lightbox="roadtrip"><img src="img/data/dance-gall.jpg" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="divider-one"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="dance-gallery clearfix">
                    <h3>видео танца</h3>
                    <div class="clearfix">
                        <div class="video-area">
                            <img src="img/data/dance-gall.jpg" alt="">
                            <p>пасадобль</p>
                        </div>
                        <div class="video-area">
                            <img src="img/data/dance-gall.jpg" alt="">
                            <p>пасадобль</p>
                        </div>
                        <div class="video-area">
                            <img src="img/data/dance-gall.jpg" alt="">
                            <p>пасадобль</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include "footer.php"; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/flexmenu.min.js"></script>
    <script src="js/jquery.plugin.js"></script>
    <script src="js/lightbox.js"></script>
    <script src="js/jquery.countdown.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>