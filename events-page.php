<!DOCTYPE html>
<html lang="en">
  <?php include "head.php"; ?>
  <body>
  	<?php include "header.php"; ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="main-h1">события</h2>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb text-center">
                    <li><a href="#">Главная</a></li>
                    <li class="active">события</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="divider"></div>
    <div class="submenu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="navbar">
                        <div class="navbar-header">
                            <button class="btn navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">Меню</button>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav no-float">
                                <li class="nav active"><a href="#">Урок со звездой</a></li>
                                <li class="nav"><a href="#">Pro-am</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="events-page-h2">Друзья! мы Приглашаем всех желаюших посетить яркие и незабываемые вечеринки вместе с нами!</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 events-page-container clearfix">
                <a href="#" class="events-page-item">
                    <div class="events-page-item-inner">
                        <div class="events-page-item-area" style="background: url('img/data/halloween.jpg');">
                            <div class="events-page-item-inner-top">
                                <h2>Ночь всех святых 2015</h2>
                                <p>Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные  </p>
                            </div>
                        </div>
                        
                        <div class="events-page-item-inner-bottom">
                            <h3>10 декабря в 20:00 в dance school</h3>
                        </div>
                    </div>
                </a>
                <a href="#" class="events-page-item">
                    <div class="events-page-item-inner">
                        <div class="events-page-item-area" style="background: url('img/data/halloween.jpg');">
                            <div class="events-page-item-inner-top">
                                <h2>Ночь всех святых 2015</h2>
                                <p>Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные  </p>
                            </div>
                        </div>
                        
                        <div class="events-page-item-inner-bottom">
                            <h3>10 декабря в 20:00 в dance school</h3>
                        </div>
                    </div>
                </a>
                <a href="#" class="events-page-item">
                    <div class="events-page-item-inner">
                        <div class="events-page-item-area" style="background: url('img/data/halloween.jpg');">
                            <div class="events-page-item-inner-top">
                                <h2>Ночь всех святых 2015</h2>
                                <p>Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные  </p>
                            </div>
                        </div>
                        
                        <div class="events-page-item-inner-bottom">
                            <h3>10 декабря в 20:00 в dance school</h3>
                        </div>
                    </div>
                </a>
                <a href="#" class="events-page-item">
                    <div class="events-page-item-inner">
                        <div class="events-page-item-area" style="background: url('img/data/halloween.jpg');">
                            <div class="events-page-item-inner-top">
                                <h2>Ночь всех святых 2015</h2>
                                <p>Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные  </p>
                            </div>
                        </div>
                        
                        <div class="events-page-item-inner-bottom">
                            <h3>10 декабря в 20:00 в dance school</h3>
                        </div>
                    </div>
                </a>
                <a href="#" class="events-page-item">
                    <div class="events-page-item-inner">
                        <div class="events-page-item-area" style="background: url('img/data/halloween.jpg');">
                            <div class="events-page-item-inner-top">
                                <h2>Ночь всех святых 2015</h2>
                                <p>Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные  </p>
                            </div>
                        </div>
                        
                        <div class="events-page-item-inner-bottom">
                            <h3>10 декабря в 20:00 в dance school</h3>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <?php include "footer.php"; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/flexmenu.min.js"></script>
    <script src="js/jquery.plugin.js"></script>
    <script src="js/jquery.countdown.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>