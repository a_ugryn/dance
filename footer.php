<div class="subscribe-container parallax-window" data-parallax="scroll" data-image-src="/img/bg/subscribe.jpg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h3>Хочешь знать когда будут лучшие вечеринки?</h3>
				<p>Оставь заявку и будь в курсе событий!</p>
			</div>
		</div>
		<div class="row">
			<form action="">
				<div class="col-lg-7 col-md-7 col-sm-7">
					<input class="pull-right" placeholder="Введите Ваш E-mail...">
				</div>
				<div class="col-lg-5 col-md-5 col-sm-5">
					<div class="button">
    					<div class="button-border">
    						<button class="button-inner">Заказать звонок</button>
    					</div>
    				</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="footer-container">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<ul class="footer-nav">
					<li class="item"><a href="#">Преподаватели</a></li>
					<li class="item"><a href="#">Расписание</a></li>
					<li class="item"><a href="#">Направление</a></li>
					<li class="item"><a href="#">События</a></li>
					<li class="item"><a href="#">Клубные карты</a></li>
					<li class="item"><a href="#">Партнеры</a></li>
					<li class="item"><a href="#">Акции</a></li>
					<li class="item"><a href="#">Контакты</a></li>
				</ul>
			</div>
			<div class="col-lg-4 col-md-4 text-center">
				<img class="footer-logo" src="img/logo.png" alt="" />
			</div>
			<div class="col-lg-4 col-md-4 footer-social">
				<div class="foter-social-margin clearfix pull-right">
					<h3>Присоединяйтесь к нам</h3>
					<ul>
						<li><a class="instagram" href="#"></a></li>
						<li><a class="facebook" href="#"></a></li>
						<li><a class="youtube" href="#"></a></li>
						<li><a class="vk" href="#"></a></li>
					</ul>
				</div>
				
			</div>
		</div>
	</div>
	<div class="copyright-container">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4">
					<p>© 2013-2016 Школа танцев  Dance School.</p> 
				</div>
				<div class="col-lg-4 col-md-4 text-center">
					<a href="#">Политика конфиденциальности</a>
				</div>
				<div class="col-lg-4 col-md-4 site-maker">
					<p class="clearfix pull-right">Разработка сайта: <a href="#">Abudonnyi Design</a></p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="call-to-less">
	<div class="call-us-popup-2">
    	<div class="call-us-popup-inner-2">
    		<img src="img/close.png" alt="" />
    		<form action="">
    			<p><input placeholder="Введите Ваше имя..." /></p>
	    		<p><input placeholder="Введите Ваш телефон..." /></p>
	    		<span class="text-center popup-button-margin">
	    			<div class="button pop">
	    				<div class="button-border">
	    					<button class="button-inner">Хочу на урок</button>
	    				</div>
	    			</div>
	    		</span>		
    		</form>	
    	</div>
    </div>
</div>

<div class="call-to-card">
	<div class="call-us-popup-3">
    	<div class="call-us-popup-inner-3">
    		<img src="img/close.png" alt="" />
    		<form action="">
    			<p><input value="" id="forCardType" disabled></p>
    			<p><input placeholder="Введите Ваше имя..." /></p>
	    		<p><input placeholder="Введите Ваш телефон..." /></p>
	    		<span class="text-center popup-button-margin">
	    			<div class="button pop">
	    				<div class="button-border">
	    					<button class="button-inner">Хочу на урок</button>
	    				</div>
	    			</div>
	    		</span>		
    		</form>	
    	</div>
    </div>
</div>