<!DOCTYPE html>
<html lang="en">
  <?php include "head.php"; ?>
  <body>
  	<?php include "header.php"; ?>
    <!-- EVENTS -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="main-h1">спецпредложения</h2>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb text-center">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Направления</a></li>
                    <li><a href="#">Бальные танцы латиноамериканская программа</a></li>
                    <li class="active">Пасадобль</li>
                </ol>
            </div>
        </div>
    </div>

    <div class="divider-one"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="dance-gallery clearfix">
                    <h3>фотографии танца</h3>
                    
                    <div class="gallery-slider">
                        <div class="gallery-slider-inner" style="background: url('img/gall/img-1.jpg');">
                            <img src="img/gall/arrow-left.png" class="prev-cg" />
                            <img src="img/gall/arrow-right.png" class="next-cg" />
                        </div>
                    </div>

                    <div class="clearfix text-center gallery-slider-img">
                        <img class="active" src="img/data/dance-gall.jpg" data-img="url('img/gall/img-1.jpg')" />
                        <img src="img/data/dance-gall.jpg" data-img="url('img/gall/img-2.jpg')" />
                        <img src="img/data/dance-gall.jpg" data-img="url('img/gall/img-1.jpg')" />
                        <img src="img/data/dance-gall.jpg" data-img="url('img/gall/img-1.jpg')" />
                        <img src="img/data/dance-gall.jpg" data-img="url('img/gall/img-1.jpg')" />
                        <img src="img/data/dance-gall.jpg" data-img="url('img/gall/img-1.jpg')" />
                        <img src="img/data/dance-gall.jpg" data-img="url('img/gall/img-1.jpg')" />
                        <img src="img/data/dance-gall.jpg" data-img="url('img/gall/img-1.jpg')" />
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="divider-one"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="dance-gallery clearfix">
                    <h3>видео танца</h3>
                    <div class="clearfix">
                        <div class="video-area">
                            <img src="img/data/dance-gall.jpg" alt="">
                            <p>пасадобль</p>
                        </div>
                        <div class="video-area">
                            <img src="img/data/dance-gall.jpg" alt="">
                            <p>пасадобль</p>
                        </div>
                        <div class="video-area">
                            <img src="img/data/dance-gall.jpg" alt="">
                            <p>пасадобль</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include "footer.php"; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/flexmenu.min.js"></script>
    <script src="js/jquery.plugin.js"></script>
    <script src="js/lightbox.js"></script>
    <script src="js/jquery.countdown.js"></script>
    <script src="js/script.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var countImg = $(".gallery-slider-img img").length;
            $(".gallery-slider-img img").click(function(){
                $(".gallery-slider-img img").removeClass("active");
                $(this).addClass("active");
                var imgCache = $(this).data("img");
                $(".gallery-slider-inner").css("background", imgCache);
            });

            $("img.next-cg").click(function(){
                var imgPos = ($(".gallery-slider-img img.active").index())+1;
                if (imgPos < countImg) {
                    $(".gallery-slider-img img.active").removeClass("active").next().addClass("active");
                    var imgCache = $(".gallery-slider-img img.active").data("img");
                    $(".gallery-slider-inner").css("background", imgCache);
                }
                if (imgPos == countImg) {
                   console.log("end");
                }
            });
            $("img.prev-cg").click(function(){
                var imgPos = ($(".gallery-slider-img img.active").index())+1;
                if (imgPos !== 1) {
                    $(".gallery-slider-img img.active").removeClass("active").prev().addClass("active");
                    var imgCache = $(".gallery-slider-img img.active").data("img");
                    $(".gallery-slider-inner").css("background", imgCache);
                }
                if (imgPos == 1) {
                   console.log("start");
                }
            });
        })
    </script>
  </body>
</html>