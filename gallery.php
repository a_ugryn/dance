<!DOCTYPE html>
<html lang="en">
  <?php include "head.php"; ?>
  <body>
  	<?php include "header.php"; ?>
    <!-- EVENTS -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="main-h1">спецпредложения</h2>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb text-center">
                    <li><a href="#">Главная</a></li>
                    <li class="active">Спецпредложения</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="divider"></div>
    <div class="submenu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="navbar">
                        <div class="navbar-header">
                            <button class="btn navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">Меню</button>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav no-float">
                                <li class="nav active"><a href="#">Урок со звездой</a></li>
                                <li class="nav"><a href="#">Pro-am</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="events clearfix">
                    <a href="#" class="events-item">
                        <img src="img/data/halloween.jpg" alt="" />
                        <span class="mask"></span>
                        <p>Название акции <br>если она в две строки</p>
                    </a>
                    <a href="#" class="events-item">
                        <img src="img/data/halloween.jpg" alt="" />
                        <span class="mask"></span>
                        <p>Название акции</p>
                    </a>
                    <a href="#" class="events-item">
                        <img src="img/data/halloween.jpg" alt="" />
                        <span class="mask"></span>
                        <p>Название акции</p>
                    </a>
                    <a href="#" class="events-item">
                        <img src="img/data/halloween.jpg" alt="" />
                        <span class="mask"></span>
                        <p>Название акции</p>
                    </a>
                    <a href="#" class="events-item">
                        <img src="img/data/halloween.jpg" alt="" />
                        <span class="mask"></span>
                        <p>Название акции</p>
                    </a>
                    <a href="#" class="events-item">
                        <img src="img/data/halloween.jpg" alt="" />
                        <span class="mask"></span>
                        <p>Название акции</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php include "footer.php"; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>