<!DOCTYPE html>
<html lang="en">
  <?php include "head.php"; ?>
  <body>
  	<?php include "header.php"; ?>
    <!-- EVENTS -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="main-h1">расписание</h2>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb text-center">
                    <li><a href="#">Главная</a></li>
                    <li class="active">Расписание</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="calendar">
                    <!-- header -->
                    <div class="calendar-row calendar-row-header clearfix">
                        <div class="calendar-row-col"></div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">Понедельник</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">Вторник</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">Среда</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">Четверг</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">Среда</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">Пятница</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">Суббота</div>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="calendar-row clearfix">
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">12:00</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner stretching">Stretching <br><span>Зенцева</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="calendar-row clearfix">
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">13:00</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner strip-dance">Strip Dance <br><span>Романова</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="calendar-row clearfix">
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">14:00</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner arg-tango">Arg.Tango (beginner) <br><span>Балаев</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner american-smooth">American Smooth <br><span>Строков</span></div>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="calendar-row clearfix">
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">15:00</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner arg-tango">Arg.Tango (w.t.3) <br><span>Романова</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner latin-b">Latin (bronze) <br><span>Строков</span></div>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="calendar-row clearfix">
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">16:00</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner ballet">Ballet (k.c.1) <br><span>Зенцева</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner ballet">Ballet (k.c.1) <br><span>Зенцева</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner ballet">Ballet (k.c.1) <br><span>Зенцева</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="calendar-row clearfix">
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">16:00</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="plus-half">
                                <div class="calendar-row-col-inner latin-d">Latin (diamond) <br><span>Поляков</span></div>
                            </div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="calendar-row clearfix">
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">17:00</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner break-dance">Break Dance <br><span>Ганачьян</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner break-dance">Break Dance <br><span>Ганачьян</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner kids-class">Kids Class (c 10 лет) <br><span>Романова</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                    </div>

                    <!-- row -->
                    <div class="calendar-row clearfix">
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">18:00</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner baby-class">Baby (Class 2) <br><span>Зенцева</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner kids-class-1">Kids (Class 1) <br><span>Романова</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner baby-class">Baby (Class 2) <br><span>Зенцева</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner kids-class-1">Kids (Class 1) <br><span>Романова</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner baby-class">Baby (Class 2) <br><span>Зенцева</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="calendar-row clearfix">
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">18:00</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner kids-class">Kids Class (c 10 лет) <br><span>Романова</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner choreography">Choreography <br>(continue)<br><span>Зенцева</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner theatre">Театр танца (Kids 1) <br><span>Франдетти</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="calendar-row clearfix">
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">19:00</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner latin-b">Latin (bronze) <br><span>Строков</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner latin-b">Latin (silver) <br><span>Балаев</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner latin-b">Latin (bronze) <br><span>Голубов</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner latin-b">Latin (silver) <br><span>Голубов</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="calendar-row clearfix">
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">19:00</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner ballet-class">Ballet Class <br><span>Зенцева</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner theatre">Театр танца <br><span>Франдетти</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner ballet-class">Ballet Class <br><span>Зенцева</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner hip-hop">Hip-Hop (Kids) <br><span>Соколова</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner ballet-class">Ballet Class <br>(continue) <br><span>Сапрон</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="calendar-row clearfix">
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">20:00</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner arg-tango">Arg.Tango (beginner) <br><span>Балаев / Романова</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner arg-tango">Arg.Tango <br><span>(практика 4)</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner arg-tango">Arg.Tango (beginner) <br><span>Балаев / Романова</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner solo-latin">Solo Latin <br><span>Романова</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner arg-tango">Arg.Tango <br><span>Открытая практика</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="calendar-row clearfix">
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner">21:00</div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner caribbean">Caribbean Mix <br>(beginner) <br><span>Романова</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner-half-area">
                                <div class="calendar-row-col-inner calendar-row-col-inner-half caribbean">Caribbean <div class="more-fn">(cont..)</div><br><span>Голубов</span></div>
                            </div>
                            <div class="calendar-row-col-inner-half-area">
                                <div class="calendar-row-col-inner calendar-row-col-inner-half strip">Strip Dance<br><span>Романова</span></div>
                            </div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner smooth">American Smooth <br><span>Голубов</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner caribbean">Caribbean Mix <br>(beginner) <br><span>Голубов</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner caribbean">Caribbean Mix <br>(beginner) <br><span>(практика 4)</span></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                        <div class="calendar-row-col">
                            <div class="calendar-row-col-inner"></div>
                        </div>
                    </div>
                </div>

                <div class="calendar-sm">
                    <ul>
                        <li>
                            <div class="calendar-day">Понедельник</div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">16:00</div>
                                <div class="calendar-area-lesson">Ballet (k.c.1) <span>Зенцева</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">18:00</div>
                                <div class="calendar-area-lesson">Baby (Class 2) <span>Зенцева</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">19:00</div>
                                <div class="calendar-area-lesson">Latin (bronze) <span>Строков</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">19:00</div>
                                <div class="calendar-area-lesson">Ballet Class <span>Зенцева</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">20:00</div>
                                <div class="calendar-area-lesson">Arg.Tango (beginner) <span>Балаев / Романова</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">21:00</div>
                                <div class="calendar-area-lesson">Caribbean Mix (beginner) <span>Романова</span></div>
                            </div>
                        </li>
                        <li>
                            <div class="calendar-day">Вторник</div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">17:00</div>
                                <div class="calendar-area-lesson">Break Dance <span>Ганачьян</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">18:00</div>
                                <div class="calendar-area-lesson">Kids (Class 1) <span>Романова</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">19:00</div>
                                <div class="calendar-area-lesson">Театр танца <span>Франдетти</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">20:00</div>
                                <div class="calendar-area-lesson">Arg.Tango <span>(практика 4)</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">21:00</div>
                                <div class="calendar-area-lesson">Caribbean Mix (cont..) <span>Голубов</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">21:00</div>
                                <div class="calendar-area-lesson">Strip Dance <span>Романова</span></div>
                            </div>
                        </li>

                         <li>
                            <div class="calendar-day">Среда</div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">18:00</div>
                                <div class="calendar-area-lesson">Baby (Class 2) <span>Зенцева</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">18:00</div>
                                <div class="calendar-area-lesson">Kids Class (c 10 лет) <span>Романова</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">19:00</div>
                                <div class="calendar-area-lesson">Latin (silver) <span>Балаев</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">19:00</div>
                                <div class="calendar-area-lesson">Ballet Class <span>Зенцева</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">20:00</div>
                                <div class="calendar-area-lesson">Arg.Tango (beginner) <span>Балаев / Романова</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">21:00</div>
                                <div class="calendar-area-lesson">American Smooth <span>Голубов</span></div>
                            </div>
                        </li>
                        <li>
                            <div class="calendar-day">Четверг</div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">16:00</div>
                                <div class="calendar-area-lesson">Ballet (k.c.1) <span>Зенцева</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">17:00</div>
                                <div class="calendar-area-lesson">Break Dance <span>Ганачьян</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">18:00</div>
                                <div class="calendar-area-lesson">Kids (Class 1) <span>Романова</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">18:00</div>
                                <div class="calendar-area-lesson">Choreography (continue) <span>Зенцева</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">19:00</div>
                                <div class="calendar-area-lesson">Latin (bronze) <span>Голубов</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">19:00</div>
                                <div class="calendar-area-lesson">Hip-Hop (Kids) <span>Соколова</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">20:00</div>
                                <div class="calendar-area-lesson">Solo Latin <span>Романова</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">21:00</div>
                                <div class="calendar-area-lesson">Caribbean Mix (beginner) <span>Голубов</span></div>
                            </div>
                        </li>
                        <li>
                            <div class="calendar-day">Пятница</div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">17:00</div>
                                <div class="calendar-area-lesson">Kids Class (c 10 лет) <span>Романова</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">18:00</div>
                                <div class="calendar-area-lesson">Baby (Class 2) <span>Зенцева</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">18:00</div>
                                <div class="calendar-area-lesson">Театр танца (Kids 1) <span>Франдетти</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">19:00</div>
                                <div class="calendar-area-lesson">Latin (silver) <span>Голубов</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">19:00</div>
                                <div class="calendar-area-lesson">Ballet Class (continue) <span>Сапрон</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">19:30</div>
                                <div class="calendar-area-lesson">Arg.Tango <span>Открытая практика</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">21:00</div>
                                <div class="calendar-area-lesson">Caribbean Mix <span>(практика 4)</span></div>
                            </div>
                        </li>
                        <li>
                            <div class="calendar-day">Суббота</div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">12:00</div>
                                <div class="calendar-area-lesson">Stretching <span>Зенцева</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">13:00</div>
                                <div class="calendar-area-lesson">Strip Dance <span>Романова</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">14:00</div>
                                <div class="calendar-area-lesson">Arg.Tango (beginner) <span>Балаев</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">15:00</div>
                                <div class="calendar-area-lesson">Arg.Tango (w.t.3) <span>Романова</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">16:00</div>
                                <div class="calendar-area-lesson">Ballet (k.c.1) <span>Зенцева</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">16:00</div>
                                <div class="calendar-area-lesson">Latin (diamond) <span>Поляков</span></div>
                            </div>
                        </li>
                        <li>
                            <div class="calendar-day">Воскресенье</div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">14:00</div>
                                <div class="calendar-area-lesson">American Smooth <span>Строков</span></div>
                            </div>
                            <div class="calendar-area clearfix">
                                <div class="calendar-area-time">15:00</div>
                                <div class="calendar-area-lesson">Latin (bronze) <span>Строков</span></div>
                            </div>
                        </li>
                    </ul>
                </div>


            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 col-md-6 col-sm-6 col-md-offset-3">
                <p class="tip">*по предварительной записи <br>1 от 6 до 9 лет 2 от 3 до 6 лет 3 женская техника 4 практика является доп. услугой</p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <span class="popup-button-margin clearfix">
                    <div class="button pull-right calendar-button">
                        <div class="button-border">
                            <button class="button-inner">Скачать расписание</button>
                        </div>
                    </div>
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 cal-marg">
                <span class="text-center popup-button-margin">
                    <div class="button">
                        <div class="button-border">
                            <button class="button-inner">Заказать звонок</button>
                        </div>
                    </div>
                </span>
            </div>
        </div>
    </div>
    
    <?php include "footer.php"; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>