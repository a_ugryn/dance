<!-- HEADER -->
<div class="header-container">
    <div class="container">
    	<div class="row">
            <div class="col-sm-12 header-logo-lg visible-xs">
                <div class="logo">
                    <img src="img/logo.png" alt="" />
                </div>
            </div>
    		<div class="col-lg-4 col-md-4 col-sm-6 header-column">
    			<div class="city-change">
    				Ростов-на-Дону
                    <div class="change-city">
                        <span>Ростов-на-Дону</span>
                        <a href="#">Москва</a>
                    </div>
    			</div>
    		</div>
    		<div class="col-lg-4 col-md-5 col-sm-6 header-column pull-right">
    			<div class="header-social">
    				<div class="header-social-links">
    					<ul>
    						<li><a class="instagram" href="#"></a></li>
    						<li><a class="facebook" href="#"></a></li>
    						<li><a class="youtube" href="#"></a></li>
    						<li><a class="vk" href="#"></a></li>
    					</ul>
    				</div>
    				<div class="header-social-phone">
    					<span>8 (954) 213 99 88</span>
    					<div class="call-us active">
    						<div><span>Заказать звонок</span></div>
    					</div>
    				</div>
    				<div class="call-us-popup">
    					<div class="call-us-popup-inner">
                            <img src="assets/theme/img/close.png" alt="">
    						<p><input placeholder="Введите Ваше имя..." /></p>
    						<p><input placeholder="Введите Ваш телефон..." /></p>
    						<span class="text-center popup-button-margin">
    							<div class="button">
    								<div class="button-border">
    									<button class="button-inner">Заказать звонок</button>
    								</div>
    							</div>
    						</span>	
    					</div>
    				</div>
    			</div>
    		</div>
            <div class="col-lg-4 col-md-3 col-sm-12 header-logo-lg hidden-xs">
                <div class="logo">
                    <img src="img/logo.png" alt="" />
                </div>
            </div>
    	</div>
    </div>
</div>

<!-- TOP MENU -->
<div class="menu-container">
    <div class="container">
    	<div class="row">
    		<div class="col-lg-12">
    			<div class="nav-menu clearfix">
                    <a href="#" class="your-card pull-right"><span>закажи свою карту</span></a>
    				<ul class="menu flex">
    					<li><a href="#">Преподаватели</a></li>
    					<li><a href="#">Направления</a></li>
    					<li><a href="#">Клубные карты</a></li>
    					<li><a href="#">Спецпредложения</a></li>
    					<li><a href="#">Расписание</a></li>
    					<li><a href="#">События</a></li>
    					<li><a href="#">Партнеры</a></li>
    					<li><a href="#">Контакты</a></li>
    				</ul>
    			</div>
    		</div>
    	</div>
    </div>
</div>