<!DOCTYPE html>
<html lang="en">
  <?php include "head.php"; ?>
  <body>
  	<?php include "header.php"; ?>
    <!-- TOP SLIDER -->
    <div class="top-slider">
        <div class="top-slider-left" style="background: url('img/bg/top-left.jpg');">
            <div class="top-slider-right" style="background: url('img/bg/top-right.jpg');">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="top-slider-container">
                                <li class="slide-area">
                                    <div class="slide-area-inner slide-area-inner-1">
                                        
                                    </div>
                                </li>
                                <li class="slide-area">
                                    <div class="slide-area-inner slide-area-inner-2">
                                        
                                    </div>
                                </li>
                                <li class="slide-area">
                                    <div class="slide-area-inner slide-area-inner-3">
                                        
                                    </div>
                                </li>
                                <li class="slide-area">
                                    <div class="slide-area-inner slide-area-inner-4">
                                        
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- DANCE CAROUSEL -->
    <div class="container">
    	<div class="row">
    		<div class="col-lg-12">
    			<h1 class="main-h1">танцуйте с нами</h1>
    			<div class="text-center">
                    <a href="#" class="main-subtitle">Все направления</a>
                </div>
    			<div class="home-carousel">
    				<div class="item">
    					<a href="#">
                            <div class="item-bg-area">
                                <div class="mask"></div>
                                <div class="item-bg" style="background: url('img/pic/pic-1.jpg');"></div>
                            </div> 
    						<!--img src="img/pic/pic-1.jpg" alt="" /-->
    						<span class="item-name">Caribbean mix</span>
    					</a>
    				</div>
    				<div class="item">
    					<a href="#">
                            <div class="item-bg-area">
                                <div class="mask"></div>
                                <div class="item-bg" style="background: url('img/pic/pic-2.jpg');"></div>
                            </div>
    						<!-- <img src="img/pic/pic-2.jpg" alt="" /> -->
    						<span class="item-name">Хореография</span>
    					</a>
    				</div>
    				<div class="item">
    					<a href="#">
                            <div class="item-bg-area">
                                <div class="mask"></div>
                                <div class="item-bg" style="background: url('img/pic/pic-3.jpg');"></div>
                            </div>
    						<!-- <img src="img/pic/pic-3.jpg" alt="" /> -->
    						<span class="item-name">Strip dance</span>
    					</a>
    				</div>
    				<div class="item">
    					<a href="#">
                            <div class="item-bg-area">
                                <div class="mask"></div>
                                <div class="item-bg" style="background: url('img/pic/pic-4.jpg');"></div>
                            </div>
    						<!-- <img src="img/pic/pic-4.jpg" alt="" /> -->
    						<span class="item-name">Аргентинское танго</span>
    					</a>
    				</div>
    				<div class="item">
    					<a href="#">
                            <div class="item-bg-area">
                                <div class="mask"></div>
                                <div class="item-bg" style="background: url('img/pic/pic-1.jpg');"></div>
                            </div>
    						<!-- <img src="img/pic/pic-1.jpg" alt="" /> -->
    						<span class="item-name">Caribbean mix</span>
    					</a>
    				</div>
    				<div class="item">
    					<a href="#">
                            <div class="item-bg-area">
                                <div class="mask"></div>
                                <div class="item-bg" style="background: url('img/pic/pic-2.jpg');"></div>
                            </div>
    						<!-- <img src="img/pic/pic-2.jpg" alt="" /> -->
    						<span class="item-name">Хореография</span>
    					</a>
    				</div>
    			</div>
    		</div>
    	</div>
    	<div class="row">
    		<span class="text-center popup-button-margin">
    			<div class="button">
    				<div class="button-border">
    					<button class="button-inner call-to-less-button"><span>Бесплатный урок</span></button>
    				</div>
    			</div>
    		</span>
    	</div>
    	<div class="row">
    		<div class="col-lg-12 advantage-area">
    			<div class="advantage">
	    			<img src="img/pic/img-1.jpg" alt="" />
	    			<p>Уникальные методики <br>преподавания</p>
	    		</div>
                <div class="advantage">
	    			<img src="img/pic/img-2.jpg" alt="" />
	    			<p>Команда профессиональных <br>преподавателей</p>
	    		</div>
	    		<div class="advantage">
	    			<img src="img/pic/img-3.jpg" alt="" />
	    			<p>Тематические вечеринки в <br>приятном обществе</p>
	    		</div>
	    		<div class="advantage">
	    			<img src="img/pic/img-4.jpg" alt="" />
	    			<p>Большой выбор групповых и <br>индивидуальных занятий</p>
	    		</div>
	    		<div class="advantage">
	    			<img src="img/pic/img-5.jpg" alt="" />
	    			<p>Уютный классический <br>интерьер, в стиле старой Ангии</p>
	    		</div>
	    		<div class="advantage">
	    			<img src="img/pic/img-6.jpg" alt="" />
	    			<p>Круглосуточная атмосфера <br>тепла и уюта</p>
	    		</div>
    		</div>
    	</div>
    </div>
    <div class="divider"></div>
    <div class="container">
    	<div class="row">
			<div class="col-lg-12">
				<h1 class="main-h1">События dance school</h1>
			</div>
    	</div>
        <div class="row">
            <div class="col-lg-12 main-events-slider">
                <div id="carousel-example-generic" class="events-slider carousel slide pull-left" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <a href="#" class="item active">
                            <img src="img/data/halloween-2.jpg" alt="" />
                            <div class="carousel-caption">
                                <h3>Ночь всех святых 2015</h3>
                                <p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более станд...</p>
                            </div>
                            <div class="slider-footer clearfix">
                            <div class="defaultCountdown defaultCountdown1 pull-left"></div>
                                <div class="slider-footer-date pull-right">10 декабря в 20:00 в dance school</div>
                            </div>
                        </a>
                        <a href="#" class="item">
                            <img src="img/data/halloween-2.jpg" alt="" />
                            <div class="carousel-caption">
                                <h3>Ночь всех святых 2015</h3>
                                <p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более станд...</p>
                            </div>
                            <div class="slider-footer clearfix">
                                <div class="defaultCountdown defaultCountdown2 pull-left"></div>
                                <div class="slider-footer-date pull-right">10 декабря в 20:00 в dance school</div>
                            </div>
                        </a>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="sr-only"><img src='img/arrow-left.png' /></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="sr-only"><img src='img/arrow-right.png' /></span>
                    </a>
                </div>
                <div class="main-last-news pull-right">
                    <a href="#" class="main-last-news-item clearfix">
                        <img src="img/data/last-news-1.jpg" alt="" />
                        <h3>ГАЛА-ШОУ "БАЛ ЦВЕТОВ"</h3>
                        <p>Вот и состоялось грандиозное событие этого лета Мировые звезды танцев в гала шоу  Бал цветов». Гос ...</p>
                        <span class="main-last-news-date">08 июня 2014</span>
                    </a>
                    <a href="#" class="main-last-news-item clearfix">
                        <img src="img/data/last-news-1.jpg" alt="" />
                        <h3>ГАЛА-ШОУ "БАЛ ЦВЕТОВ"</h3>
                        <p>Вот и состоялось грандиозное событие этого лета Мировые звезды танцев в гала шоу  Бал цветов». Гос ...</p>
                        <span class="main-last-news-date">08 июня 2014</span>
                    </a>
                    <a href="#" class="main-last-news-item clearfix">
                        <img src="img/data/last-news-1.jpg" alt="" />
                        <h3>ГАЛА-ШОУ "БАЛ ЦВЕТОВ"</h3>
                        <p>Вот и состоялось грандиозное событие этого лета Мировые звезды танцев в гала шоу  Бал цветов». Гос ...</p>
                        <span class="main-last-news-date">08 июня 2014</span>
                    </a>
                    <a href="#" class="more-news">Все новости</a>
                </div>  
            </div>
            
        </div>
    </div>
    <div class="divider"></div>
    <div class="container teacher-main-container">
    	<div class="row">
    		<div class="col-lg-12">
    			<h1 class="main-h1">наши преподаватели</h1>
                <div class="text-center">
                    <a href="#" class="main-subtitle">все преподаватели</a>
                </div>
    		</div>
    		<div class="col-lg-12 text-center">
    			<div class="our-teachers our-teachers-1">
                    <a href="#">
                        <div class="mask"></div>
    				    <div class="our-teachers-info">
    					    <div class="our-teachers-info-inner golden-border">
    						    <div class="teach-info golden-border">
    						       <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
    							   <img src="img/dev.png" alt="">
    							   <p>Основатель DANCE SCHOOL <br>Бальные латиноамериканские танцы</p>
    						    </div>
    					    </div>
    				    </div>
                    </a>
    			</div>
    			<div class="our-teachers our-teachers-2">
                    <a href="#">
                        <div class="mask"></div>
    				    <div class="our-teachers-info">
    					    <div class="our-teachers-info-inner golden-border">
    						    <div class="teach-info golden-border">
    						       <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
    							   <img src="img/dev.png" alt="">
    							   <p>Основатель DANCE SCHOOL <br>Бальные латиноамериканские танцы</p>
    						    </div>
    					    </div>
    				    </div>
                    </a>
    			</div>
    			<div class="our-teachers our-teachers-3">
                    <a href="#">
                        <div class="mask"></div>
        				<div class="our-teachers-info">
        					<div class="our-teachers-info-inner golden-border">
        						<div class="teach-info golden-border">
        							<h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
        							<img src="img/dev.png" alt="">
        							<p>Основатель DANCE SCHOOL <br>Бальные латиноамериканские танцы</p>
        						</div>
        					</div>
        				</div>
                    </a>
    			</div>
    			<div class="our-teachers our-teachers-4">
                    <a href="#">
                        <div class="mask"></div>
    				    <div class="our-teachers-info">
    					    <div class="our-teachers-info-inner golden-border">
    						    <div class="teach-info golden-border">
    						       <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
    							   <img src="img/dev.png" alt="">
    							   <p>Основатель DANCE SCHOOL <br>Бальные латиноамериканские танцы</p>
    						    </div>
    					    </div>
    				    </div>
                    </a>
    			</div>
    		</div>
    	</div>
    </div>
    <div class="divider"></div>
    <div class="container">
    	<div class="row">
    		<div class="col-lg-12 brand-carousel clearfix">
    			<div class="brand-carousel-item">
                    <img src="img/brand/cinnabon.png" alt="" />         
                </div>
                <div class="brand-carousel-item">
                    <img src="img/brand/monte.png" alt="" />         
                </div>
                <div class="brand-carousel-item">
                    <img src="img/brand/charly.png" alt="" />         
                </div>
                <div class="brand-carousel-item">
                    <img src="img/brand/shokolad.png" alt="" />         
                </div>
                <div class="brand-carousel-item">
                    <img src="img/brand/sobaka.png" alt="" />         
                </div>
    		</div>
    	</div>
    </div>
    <div class="divider"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="main-h1">о dance school</h1>
				<h2 class="main-h2 text-center main-h2-margin"><em>«Жизнь танца настолько коротка, что не хватит и всей жизни. Не теряйте время...» <br>Александр Поляков</em></h2>
				<div class="main-page-content">
					<p>Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные клубные танцы или классические бальные танцы. Всё зависит от ваших вкусов и предпочтений. В нашем коллективе есть преподаватели по хореографии, бальным танцам, латиноамериканским танцам, хип-хопу, танцу живота, стрип пластике и другим направлениям. Мы проводим занятия по обучению танцам для начинающих, профессионалов. В детском классе опытный преподаватель Надежда Романова проводит занятия по хореографии с детьми. Регулярно для выпускников проводим танцевальные вечеринки и турниры по бальным танцам на которые, приглашаем всех желающих.</p>
					<p>Идейные соображения высшего порядка, а также дальнейшее развитие различных форм деятельности позволяет выполнять важные задания по разработке существенных финансовых и административных условий. Равным образом постоянный количественный рост и сфера нашей активности представляет собой интересный эксперимент проверки форм развития. Таким образом новая модель организационной деятельности в значительной степени обуславливает создание форм развития. Повседневная практика показывает, что реализация намеченных плановых заданий играет важную роль в формировании соответствующий условий активизации. С другой стороны постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет оценить значение форм развития.</p>
				</div>
			</div>
		</div>
	</div>
    <?php include "footer.php"; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/flexmenu.min.js"></script>
    <script src="js/jquery.plugin.js"></script>
    <script src="js/jquery.countdown.js"></script>
    <script src="js/parallax.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>