$(document).ready(function(){
	$(".call-us").click(function(){
		$(".call-us-popup").fadeToggle();
        $(this).removeClass("active");
	});
    $(".header-social .call-us-popup-inner img").click(function(){
        $(".call-us-popup").fadeOut();
        $(".call-us").removeClass("active");
    });
    $("html").click(function(){
        $(".call-us-popup").fadeOut();
    })
    $('.call-us-popup, .call-us.active').click(function(event){
        event.stopPropagation();
    });
    $(".button-inner-header").click(function(){
        $(".call-us-popup").delay(1000).fadeToggle();
    });




    $(".pop .button-inner").click(function(){
        $(".call-to-less").delay(1000).fadeToggle();
    });
    $(".call-to-less").click(function(){
        $(this).fadeOut();
    });
    $('.call-us-popup-2').click(function(event){
        event.stopPropagation();
    });
    $(".card-but").click(function(){
        var thisCardData = $(this).data("card");
        console.log(thisCardData);
        $("#forCardType").val(thisCardData);
        $(".call-to-card").fadeToggle();
    });
    $(".call-to-card img").click(function(){
        $(".call-to-card").fadeToggle();
        $("#foCardType").val("");
    });
    $(".call-to-card button").click(function(){
        $(".call-to-card").delay(1000).fadeToggle();
    });
    $(".call-to-card").click(function(){
        $(this).fadeOut();
    });
    $('.call-us-popup-3').click(function(event){
        event.stopPropagation();
    });

    $('ul.menu.flex').flexMenu();
	$(".home-carousel").owlCarousel({
        loop:false,
        responsiveClass:true,
        navText: ["<img src='assets/theme/img/arrow-left.png' />","<img src='assets/theme/img/arrow-right.png' />"],
        responsive:{
            0:{items:1, nav:true},
            600:{items:2, nav:true},
            768:{items:3, nav:true},
            1000:{items:4, nav:true, loop:false}
        }
    });
    $(".brand-carousel").owlCarousel({
        responsiveClass:true,
        loop: false,
        nav: false,
        autoWidth: true,
        responsive:{
            0:{items:1},
            639:{items:2},
            992:{items:3},
            1260:{items:5, loop:false}
        }
    });
    $(".teacher-carousel").owlCarousel({
        responsiveClass:true,
        navText: ["<img src='assets/theme/img/arrow-left.png' />","<img src='assets/theme/img/arrow-right.png' />"],
        responsive:{
            0:{items:1, nav:false},
            639:{items:2, nav:false, loop:false},
            992:{items:3, nav:false, loop:false},
            1200:{items:4, nav:true, loop:false, margin: 12}
        }
    });
    $(".slide-area").mouseover(function(){
        $(".slide-area").removeClass("hovered").addClass("other-slides");
        $(this).addClass("hovered").removeClass("other-slides");
    });
    $(".slide-area").mouseout(function(){
        $(".slide-area").removeClass("hovered").removeClass("other-slides");
    });


    $(function () {
        var austDay = new Date();
        austDay = new Date(austDay.getFullYear() + 0, 10 - 7, 2);
        $('.defaultCountdown1').countdown({until: austDay,});
        $('#year').text(austDay.getFullYear());
    });
    $(function () {
        var austDay = new Date();
        austDay = new Date(austDay.getFullYear() + 0, 12 - 7, 26);
        $('.defaultCountdown2').countdown({until: austDay,});
        $('#year').text(austDay.getFullYear());
    });


    $(".call-to-less-button, .call-us-popup-inner-2 > img, .teacher-call").click(function(e){
        e.preventDefault();
        $(".call-to-less").fadeToggle();
    });



    $("#filer-min-price").blur(function(){
        var thisVal = $(this).val();
        $(".filter-content-single").each(function(){
            var thisItem = $(this).data("price");
            if (thisItem < thisVal) {
                $(this).addClass("small");
            }
            else {
                $(this).removeClass("small");
            }
        });
    });

    $("#filer-max-price").blur(function(){
        var thisVal = $(this).val();
        $(".filter-content-single").each(function(){
            var thisItem = $(this).data("price");
            if (thisItem > thisVal) {
                $(this).addClass("big");
            }
            if (thisItem == thisVal) {
                $(this).removeClass("big");
            }
             if (thisVal == 0) {
                $(this).removeClass("big");
            }
            if (thisItem < thisVal) {
                $(this).removeClass("big");
            }
        });
    });

    $("#date-card").change(function(){
        var thisVal = $(this).val();
        console.log(thisVal);
        $(".filter-content-single").each(function(){
            var thisItem = $(this).data("date");
            if (thisItem == thisVal || thisVal == 0) {
               $(this).removeClass("not-match")
            }
            else {
                $(this).addClass("not-match")
            }
        });
    });

    $("#type-card").change(function(){
        var thisVal = $(this).val();
        console.log(thisVal);
        $(".filter-content-single").each(function(){
            var thisItem = $(this).data("typee");
            if (thisItem == thisVal || thisVal == 0) {
               $(this).removeClass("not-matchis")
            }
            else {
                $(this).addClass("not-matchis")
            }
        });
    });


});