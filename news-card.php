<!DOCTYPE html>
<html lang="en">
  <?php include "head.php"; ?>
  <body>
  	<?php include "header.php"; ?>
    <!-- EVENTS -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="main-h1">спецпредложения</h2>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb text-center">
                    <li><a href="#">Главная</a></li>
                    <li class="active">Спецпредложения</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="divider"></div>
    <div class="submenu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="navbar">
                        <div class="navbar-header">
                            <button class="btn navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">Меню</button>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav no-float">
                                <li class="nav active"><a href="#">Урок со звездой</a></li>
                                <li class="nav"><a href="#">Pro-am</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container container-padding">
        <div class="row">
            <div class="col-lg-12">
                <div class="special clearfix">
                    <img class="special-img" src="img/data/halloween.jpg" alt="" />
                    <h2 class="special-h2">ГАЛА-ШОУ "ЧИКАГО 20-Х"</h2>
                    <div class="special-date">08 июня 2014</div>
                    <p>Идея создания танцевальной студии, где под одной крышей объединились профессионалы
                    в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные клубные танцы или классические бальные танцы. Всё зависит от ваших вкусов и предпочтений. В нашем коллективе есть преподаватели по хореографии, бальным танцам, латиноамериканским танцам, хип-хопу, танцу живота, стрип пластике и другим направлениям. Мы проводим занятия по обучению танцам для начинающих, профессионалов. В детском классе опытный преподаватель Надежда Романова проводит занятия по хореографии с детьми. Регулярно для выпускников проводим танцевальные вечеринки и турниры по бальным танцам на которые, приглашаем всех желающих.</p>
                    <p>Идейные соображения высшего порядка, а также дальнейшее развитие различных форм деятельности позволяет выполнять важные задания по разработке существенных финансовых и административных условий. Равным образом постоянный количественный рост и сфера нашей активности представляет собой интересный эксперимент проверки форм равтия. Таким образом новая модель организационной </p>
                    <p>Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные клубные танцы или классические бальные танцы. Всё зависит от ваших вкусов и предпочтений. В нашем коллективе есть преподаватели по хореографии, бальным танцам, латиноамериканским танцам, хип-хопу, танцу живота, стрип пластике и другим направлениям. Мы проводим занятия по обучению танцам для начинающих, профессионалов. В детском классе опытный преподаватель Надежда Романова проводит занятия по хореографии с детьми. Регулярно для выпускников проводим танцевальные вечеринки и турниры по бальным танцам на которые, приглашаем всех желающих.</p>
                    <p>Идейные соображения высшего порядка, а также дальнейшее развитие различных форм деятельности позволяет выполнять важные задания по разработке существенных финансовых и административных условий. Равным образом постоянный количественный рост и сфера нашей активности представляет собой интересный эксперимент проверки форм развития.       Таким образом новая модель организационной деятельности в значительной степени обуславливает создание форм развития. Повседневная практика показывает, что реализация намеченных плановых заданий играет важную роль в формировании </p>
                </div>
                <div class="text-center special-margin clearfix">
                    <div class="button">
                        <div class="button-border">
                            <button class="button-inner">Обратно к новостям</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include "footer.php"; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>