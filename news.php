<!DOCTYPE html>
<html lang="en">
  <?php include "head.php"; ?>
  <body>
  	<?php include "header.php"; ?>
    <!-- EVENTS -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="main-h1">новости</h2>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb text-center">
                    <li><a href="#">Главная</a></li>
                    <li class="active">Новости</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="divider"></div>
    <div class="submenu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="navbar">
                        <div class="navbar-header">
                            <button class="btn navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">Меню</button>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav no-float">
                                <li class="nav active"><a href="#">Урок со звездой</a></li>
                                <li class="nav"><a href="#">Pro-am</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container news-preview-container">
        <div class="row">
            <div class="col-lg-12">
                <div class="news-preview">
                    <img src="img/data/news-prev.jpg" alt="" />
                    <span>08 июня 2014</span>
                    <h3>ГАЛА-ШОУ "ЧИКАГО 20-Х"</h3>
                    <p>Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные клубные танцы или классические бальные танцы. Всё зависит от ваших вкусов и предпочтений. В нашем коллективе есть преподаватели...</p>
                    <a href="#">подробнее</a>
                </div>
                <div class="news-preview">
                    <img src="img/data/news-prev.jpg" alt="" />
                    <span>08 июня 2014</span>
                    <h3>ГАЛА-ШОУ "ЧИКАГО 20-Х"</h3>
                    <p>Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные клубные танцы или классические бальные танцы. Всё зависит от ваших вкусов и предпочтений. В нашем коллективе есть преподаватели...</p>
                    <a href="#">подробнее</a>
                </div>
                <div class="news-preview">
                    <img src="img/data/news-prev.jpg" alt="" />
                    <span>08 июня 2014</span>
                    <h3>ГАЛА-ШОУ "ЧИКАГО 20-Х"</h3>
                    <p>Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные клубные танцы или классические бальные танцы. Всё зависит от ваших вкусов и предпочтений. В нашем коллективе есть преподаватели...</p>
                    <a href="#">подробнее</a>
                </div>
                <div class="news-preview">
                    <img src="img/data/news-prev.jpg" alt="" />
                    <span>08 июня 2014</span>
                    <h3>ГАЛА-ШОУ "ЧИКАГО 20-Х"</h3>
                    <p>Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные клубные танцы или классические бальные танцы. Всё зависит от ваших вкусов и предпочтений. В нашем коллективе есть преподаватели...</p>
                    <a href="#">подробнее</a>
                </div>
                <div class="news-preview">
                    <img src="img/data/news-prev.jpg" alt="" />
                    <span>08 июня 2014</span>
                    <h3>ГАЛА-ШОУ "ЧИКАГО 20-Х"</h3>
                    <p>Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные клубные танцы или классические бальные танцы. Всё зависит от ваших вкусов и предпочтений. В нашем коллективе есть преподаватели...</p>
                    <a href="#">подробнее</a>
                </div>
                <div class="news-preview">
                    <img src="img/data/news-prev.jpg" alt="" />
                    <span>08 июня 2014</span>
                    <h3>ГАЛА-ШОУ "ЧИКАГО 20-Х"</h3>
                    <p>Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные клубные танцы или классические бальные танцы. Всё зависит от ваших вкусов и предпочтений. В нашем коллективе есть преподаватели...</p>
                    <a href="#">подробнее</a>
                </div>
                <div class="news-preview">
                    <img src="img/data/news-prev.jpg" alt="" />
                    <span>08 июня 2014</span>
                    <h3>ГАЛА-ШОУ "ЧИКАГО 20-Х"</h3>
                    <p>Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные клубные танцы или классические бальные танцы. Всё зависит от ваших вкусов и предпочтений. В нашем коллективе есть преподаватели...</p>
                    <a href="#">подробнее</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container news-preview-container">
        <div class="row">
            <div class="col-lg-12">
                    <ul class="pagination">
                        <li class="disabled"><span>«</span></li>
                        <li class="disabled"><span>«</span></li>
                        <li class="active"><a href="novosti/">1</a></li>
                        <li><a href="novosti/?page=2">2</a></li>
                        <li class="control"><a href="novosti/?page=2">»</a></li>
                        <li class="control"><a href="novosti/?page=2">»</a></li>
                    </ul>
            </div>
        </div>
    </div>
    <?php include "footer.php"; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>