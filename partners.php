<!DOCTYPE html>
<html lang="en">
  <?php include "head.php"; ?>
  <body>
  	<?php include "header.php"; ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="main-h1">наши партнеры</h2>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb text-center">
                    <li><a href="#">Главная</a></li>
                    <li class="active">Партнеры</li>
                </ol>
            </div>
        </div>
    </div>
    
    <div class="container partners-container">
        <div class="row">
            <div class="col-lg-12 clearfix">
                <div class="partners-container-item">
                    <div class="partners-container-item-inner clearfix">
                        <div class="partners-container-item-inner-left">
                            <div class="partners-container-item-inner-left-inner">
                                <img src="img/brand/cinnabon.png" alt="" />
                            </div>
                        </div>
                        <div class="partners-container-item-inner-right">
                            <h3>Радио "MONTE CARLO"</h3>
                            <span class="text-center popup-button-margin">
                                <div class="button">
                                    <div class="button-border">
                                        <button class="button-inner">Перейти</button>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="partners-container-item">
                    <div class="partners-container-item-inner clearfix">
                        <div class="partners-container-item-inner-left">
                            <div class="partners-container-item-inner-left-inner">
                                <img src="img/brand/cinnabon.png" alt="" />
                            </div>
                        </div>
                        <div class="partners-container-item-inner-right">
                            <h3>Радио "MONTE CARLO"</h3>
                            <span class="text-center popup-button-margin">
                                <div class="button">
                                    <div class="button-border">
                                        <button class="button-inner">Перейти</button>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="partners-container-item">
                    <div class="partners-container-item-inner clearfix">
                        <div class="partners-container-item-inner-left">
                            <div class="partners-container-item-inner-left-inner">
                                <img src="img/brand/cinnabon.png" alt="" />
                            </div>
                        </div>
                        <div class="partners-container-item-inner-right">
                            <h3>Радио "MONTE CARLO"</h3>
                            <span class="text-center popup-button-margin">
                                <div class="button">
                                    <div class="button-border">
                                        <button class="button-inner">Перейти</button>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="partners-container-item">
                    <div class="partners-container-item-inner clearfix">
                        <div class="partners-container-item-inner-left">
                            <div class="partners-container-item-inner-left-inner">
                                <img src="img/brand/cinnabon.png" alt="" />
                            </div>
                        </div>
                        <div class="partners-container-item-inner-right">
                            <h3>Радио "MONTE CARLO"</h3>
                            <span class="text-center popup-button-margin">
                                <div class="button">
                                    <div class="button-border">
                                        <button class="button-inner">Перейти</button>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="partners-container-item">
                    <div class="partners-container-item-inner clearfix">
                        <div class="partners-container-item-inner-left">
                            <div class="partners-container-item-inner-left-inner">
                                <img src="img/brand/cinnabon.png" alt="" />
                            </div>
                        </div>
                        <div class="partners-container-item-inner-right">
                            <h3>Радио "MONTE CARLO"</h3>
                            <span class="text-center popup-button-margin">
                                <div class="button">
                                    <div class="button-border">
                                        <button class="button-inner">Перейти</button>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="partners-container-item">
                    <div class="partners-container-item-inner clearfix">
                        <div class="partners-container-item-inner-left">
                            <div class="partners-container-item-inner-left-inner">
                                <img src="img/brand/cinnabon.png" alt="" />
                            </div>
                        </div>
                        <div class="partners-container-item-inner-right">
                            <h3>Радио "MONTE CARLO"</h3>
                            <span class="text-center popup-button-margin">
                                <div class="button">
                                    <div class="button-border">
                                        <button class="button-inner">Перейти</button>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>

    <?php include "footer.php"; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/flexmenu.min.js"></script>
    <script src="js/jquery.plugin.js"></script>
    <script src="js/jquery.countdown.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>