<!DOCTYPE html>
<html lang="en">
  <?php include "head.php"; ?>
  <body>
  	<?php include "header.php"; ?>
    <!-- EVENTS -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="main-h1 way-dance-h1">PRO-AM</h2>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb text-center">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Спецпредложения</a></li>     
                    <li class="active">Pro-am</li>
                </ol>
            </div>
        </div>
    </div>
    
    <div class="divider"></div>
    <div class="submenu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="navbar">
                        <div class="navbar-header">
                            <button class="btn navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">Меню</button>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav no-float">
                                <li class="nav active"><a href="#">Урок со звездой</a></li>
                                <li class="nav"><a href="#">Pro-am</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container container-padding">
        <div class="row">
            <div class="col-lg-6">
                <div class="special clearfix">
                    <h3 class="half-title">Professional - Amateur</h3>
                    <p>Professional - Amateur – новое, но уже очень популярное движение в танцевальном мире. В последние несколько лет в России активно развивается новый вид досуга для активных людей – танцы в формате Pro-Am. Танцевальная пара, состоящая из преподавателя (Professional) и ученика (Amateur), может не только танцевать и тренироваться в зале, но и принимать участие в различных танцевальных турнирах. Pro-Am соревнования проводятся как в рамках турниров по спортивным бальным танцам, так в виде командных встреч и фестивалей.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="special clearfix">
                    <h3 class="half-title-advantage">Основные преимущества занятий в формате Pro-Am:</h3>
                    <p><strong>Удобство.</strong> Pro-Am самый комфортный вид обучения для взрослых. Ваш преподаватель одновременно является и вашим партнером, система занятий планируется исходя из Ваших пожеланий и только в удобное для Вас время. <br><strong>Вовлеченность.</strong> В паре с профессионалом Вы становитесь частью насыщенного культурными событиями танцевального мира. <br><strong>Развитие.</strong> Обучение в формате Pro-Am – это неограниченные возможности совершенствования и развития, ведь рост мастерства происходит не только на тренировках, но и во время танцевальных мероприятий, конкурсов Pro-Am, проводимых по всему миру.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="single-divider"></div>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="dance-gallery clearfix">
                    <h3>фотографии танца</h3>
                    <div class="clearfix text-center">
                        <img src="img/data/dance-gall.jpg" alt="">
                        <img src="img/data/dance-gall.jpg" alt="">
                        <img src="img/data/dance-gall.jpg" alt="">
                        <img src="img/data/dance-gall.jpg" alt="">
                        <img src="img/data/dance-gall.jpg" alt="">
                        <img src="img/data/dance-gall.jpg" alt="">
                        <img src="img/data/dance-gall.jpg" alt="">
                        <img src="img/data/dance-gall.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="divider-one"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="dance-gallery clearfix">
                    <h3>видео танца</h3>
                    <div class="clearfix">
                        <div class="video-area">
                            <img src="img/data/dance-gall.jpg" alt="">
                            <p>пасадобль</p>
                        </div>
                        <div class="video-area">
                            <img src="img/data/dance-gall.jpg" alt="">
                            <p>пасадобль</p>
                        </div>
                        <div class="video-area">
                            <img src="img/data/dance-gall.jpg" alt="">
                            <p>пасадобль</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php include "footer.php"; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>