<!DOCTYPE html>
<html lang="en">
  <?php include "head.php"; ?>
  <body>
  	<?php include "header.php"; ?>
    <!-- EVENTS -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="main-h1">спецпредложения</h2>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb text-center">
                    <li><a href="#">Главная</a></li>
                    <li class="active">Спецпредложения</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="divider"></div>
    <div class="submenu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="navbar">
                        <div class="navbar-header">
                            <button class="btn navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">Меню</button>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav no-float">
                                <li class="nav active"><a href="#">Урок со звездой</a></li>
                                <li class="nav"><a href="#">Pro-am</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container container-padding">
        <div class="row">
            <div class="col-lg-12">
                <div class="special clearfix">
                    <img class="special-img" src="img/data/bebi.jpg" alt="" />
                    <h2 class="special-h2">название спецпредложения</h2>
                    <p>Идея создания танцевальной студии, где под одной крышей объединились профессионалы в различных танцевальных направлениях, давно реализована в Ростове. Dance School - танцевальный клуб, где вы можете научиться танцевать современные клубные танцы или классические бальные танцы. Всё зависит от ваших вкусов и предпочтений.</p>
                    <p>В нашем коллективе есть преподаватели по хореографии, бальным танцам, латиноамериканским танцам, хип-хопу, танцу живота, стрип пластике и другим направлениям. Мы проводим занятия по обучению танцам для начинающих, профессионалов.</p>
                    <p>В детском классе опытный преподаватель Надежда Романова проводит занятия по хореографии с детьми. Регулярно для выпускников проводим танцевальные вечеринки и турниры по бальным танцам на которые, приглашаем всех желающих.</p>
                </div>
                <div class="text-center special-margin clearfix">
                    <div class="button">
                        <div class="button-border">
                            <button class="button-inner">Заказать</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php include "footer.php"; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>