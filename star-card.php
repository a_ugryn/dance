<!DOCTYPE html>
<html lang="en">
  <?php include "head.php"; ?>
  <body>
  	<?php include "header.php"; ?>
    <!-- EVENTS -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="main-h1">спецпредложения</h2>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb text-center">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Направления</a></li>
                    <li><a href="#">Бальные танцы латиноамериканская программа</a></li>
                    <li class="active">Пасадобль</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="divider"></div>
    <div class="submenu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="navbar">
                        <div class="navbar-header">
                            <button class="btn navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">Меню</button>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav no-float">
                                <li class="nav active"><a href="#">Урок со звездой</a></li>
                                <li class="nav"><a href="#">Pro-am</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container container-padding">
        <div class="row">
            <div class="col-lg-12">
                <div class="full-width star-card">
                    <div class="half-width">
                        <img src="img/data/star.jpg" alt="" />
                    </div>
                    <div class="half-width">
                        <h3>Танцевальное направление:</h3>
                        <p>Бальные танцы - латиноамериканская программа, Постановка номеров танцев любых направлений, Латиноамериканская шоу программа</p>
                        <div class="clearfix">
                            <div class="half-width half-border">
                                <h3>Уровень:</h3>
                                <p>МАСТЕР ТАНЦА</p>
                            </div>
                            <div class="half-width half-button">
                                <a href="#">Записаться на урок</a>
                            </div>
                        </div>
                        <h3>Достижения:</h3>
                        <p>- Трехкратная Чемпионка Мира по латиноамериканскому шоу <br>- Чемпионка Америки по латиноамериканской программе.</p>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="divider-one"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="dance-gallery clearfix">
                    <span>Фотографии:</span>
                    <div class="clearfix text-center">
                        <img src="img/data/dance-gall.jpg" alt="">
                        <img src="img/data/dance-gall.jpg" alt="">
                        <img src="img/data/dance-gall.jpg" alt="">
                        <img src="img/data/dance-gall.jpg" alt="">
                        <img src="img/data/dance-gall.jpg" alt="">
                        <img src="img/data/dance-gall.jpg" alt="">
                        <img src="img/data/dance-gall.jpg" alt="">
                        <img src="img/data/dance-gall.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="divider-one"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="dance-gallery clearfix">
                    <span>видео танца:</span>
                    <div class="clearfix">
                        <div class="video-area">
                            <img src="img/data/dance-gall.jpg" alt="">
                            <p>пасадобль</p>
                        </div>
                        <div class="video-area">
                            <img src="img/data/dance-gall.jpg" alt="">
                            <p>пасадобль</p>
                        </div>
                        <div class="video-area">
                            <img src="img/data/dance-gall.jpg" alt="">
                            <p>пасадобль</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include "footer.php"; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>