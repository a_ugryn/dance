<!DOCTYPE html>
<html lang="en">
  <?php include "head.php"; ?>
  <body>
    <?php include "header.php"; ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="main-h1">наши партнеры</h2>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb text-center">
                    <li><a href="#">Главная</a></li>
                    <li class="active">Партнеры</li>
                </ol>
            </div>
        </div>
    </div>
    
    <div class="divider"></div>
    <div class="submenu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="navbar">
                        <div class="navbar-header">
                            <button class="btn navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">Меню</button>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav no-float">
                                <li class="nav active"><a href="#">Урок со звездой</a></li>
                                <li class="nav"><a href="#">Pro-am</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container teacher-main-container star-lesson">
        <div class="row">
            <div class="col-lg-12">
                <a href="#" class="our-teachers our-teachers-1">
                    <div class="mask"></div>
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="#" class="our-teachers our-teachers-2">
                    <div class="mask"></div>
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="#" class="our-teachers our-teachers-3">
                    <div class="mask"></div>
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="#" class="our-teachers our-teachers-4">
                    <div class="mask"></div>
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="#" class="our-teachers our-teachers-1">
                    <div class="mask"></div>
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="#" class="our-teachers our-teachers-2">
                    <div class="mask"></div>
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="#" class="our-teachers our-teachers-3">
                    <div class="mask"></div>
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="#" class="our-teachers our-teachers-4">
                    <div class="mask"></div>
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="#" class="our-teachers our-teachers-1">
                    <div class="mask"></div>
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="#" class="our-teachers our-teachers-2">
                    <div class="mask"></div>
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="#" class="our-teachers our-teachers-3">
                    <div class="mask"></div>
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="#" class="our-teachers our-teachers-4">
                    <div class="mask"></div>
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <?php include "footer.php"; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>