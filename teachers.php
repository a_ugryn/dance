<!DOCTYPE html>
<html lang="en">
  <?php include "head.php"; ?>
  <body>
  	<?php include "header.php"; ?>
    <!-- EVENTS -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="main-h1 way-dance-h1">Преподаватели</h2>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb text-center">
                    <li><a href="#">Главная</a></li>   
                    <li class="active">Преподаватели</li>
                </ol>
            </div>
            <p class="teachers-text-top">Дорогие друзья! Разрешите представить вам звёздную команду преподавателей DANCE SCHOOL, каждый из которых имеет свою уникальную методику в области обучения танцам и личным примером готов демонстрировать не только своё мастерство танцора, но и квалифицированного тренера.</p>
        </div>
    </div>
    <div class="divider-one"></div>
    <div class="container container-padding">
        <div class="row">
            <div class="col-lg-12 teachers-col">
                <div class="our-teachers our-teachers-1">
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                                <img src="img/dev.png" alt="">
                                <p>Основатель DANCE SCHOOL <br>Бальные латиноамериканские танцы</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="our-teachers our-teachers-2">
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                                <img src="img/dev.png" alt="">
                                <p>Основатель DANCE SCHOOL <br>Бальные латиноамериканские танцы</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="our-teachers our-teachers-3">
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                                <img src="img/dev.png" alt="">
                                <p>Основатель DANCE SCHOOL <br>Бальные латиноамериканские танцы</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="our-teachers our-teachers-4">
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                                <img src="img/dev.png" alt="">
                                <p>Основатель DANCE SCHOOL <br>Бальные латиноамериканские танцы</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="our-teachers our-teachers-1">
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                                <img src="img/dev.png" alt="">
                                <p>Основатель DANCE SCHOOL <br>Бальные латиноамериканские танцы</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="our-teachers our-teachers-2">
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                                <img src="img/dev.png" alt="">
                                <p>Основатель DANCE SCHOOL <br>Бальные латиноамериканские танцы</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="our-teachers our-teachers-3">
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                                <img src="img/dev.png" alt="">
                                <p>Основатель DANCE SCHOOL <br>Бальные латиноамериканские танцы</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="our-teachers our-teachers-4">
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                                <img src="img/dev.png" alt="">
                                <p>Основатель DANCE SCHOOL <br>Бальные латиноамериканские танцы</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="our-teachers our-teachers-1">
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                                <img src="img/dev.png" alt="">
                                <p>Основатель DANCE SCHOOL <br>Бальные латиноамериканские танцы</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="our-teachers our-teachers-2">
                    <div class="our-teachers-info">
                        <div class="our-teachers-info-inner golden-border">
                            <div class="teach-info golden-border">
                                <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                                <img src="img/dev.png" alt="">
                                <p>Основатель DANCE SCHOOL <br>Бальные латиноамериканские танцы</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include "footer.php"; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>