<!DOCTYPE html>
<html lang="en">
  <?php include "head.php"; ?>
  <body>
  	<?php include "header.php"; ?>
    <!-- EVENTS -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="main-h1 way-dance-h1">БАЛЬНЫЕ ТАНЦЫ ЛАТИНОАМЕРИКАНСКАЯ ПРОГРАММА</h2>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb text-center">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Направления</a></li>     
                    <li class="active">Бальные танцы латиноамериканская программа</li>
                </ol>
            </div>
        </div>
    </div>
    

    <div class="container container-padding">
        <div class="row">
            <div class="col-lg-6">
                <div class="special clearfix">
                    <p>Латиноамекриканские танцы возникли в далеком девятнадцатом столетии. Способствовало этому слияние культур ряда стран: Испании, Португалии, Индии. Так родилась новая культура – культура свободных, чувственных, и ритмичных латиноамериканских танцев.</p>
                    <p>К началу двадцатого века такие латина танцы как румба, джайв и пасодобль приобрели уже множество поклонников в Америке и Европе. Что касается самбы и ча-ча-ча, то они сформировались лишь во второй половине двадцатого века, и несмотря на столь позднее появление сумели прочно обосноваться в ранге самых популярных танцев мира.</p>
                    <p>Сегодня набор из этих пяти танцев составляет стандартную латиноамериканскую программу. Румба, зародившаяся на Кубе, давно стала латиноамериканской классикой – этот медленный танец, наполненный чувственными движениями, поможет рассказать историю любви между мужчиной и женщиной.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="special clearfix">
                    <p>У джайва, который произошел от свинга, другое настроение – это самый озорной и зажигательный танец программы: на соревнованиях его исполняют последним, и это неслучайно - танцоры должны показать, что они не устали и готовы отдать джайву все силы.</p>
                    <p>Пасодобль – смелый и трагический танец с испанским лицом: в нем мужчина преображается в матадора на бою с быком, а дама – в его плащ. При исполнении пасодобля используется дробь каблуков для создания ритмической интерпретации.</p>
                    <p>Что касается самбы и ча-ча-ча, то они состоят из пружинящих движений ног и бедер и по сей день пользуются огромной популярностью на только на соревнованиях, но и на любых танцевальных вечерах и дискотеках.</p>
                </div>
            </div>
        </div>
    </div>


    <div class="divider"></div>
    <div class="submenu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="navbar">
                        <div class="navbar-header">
                            <button class="btn navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">Меню</button>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav no-float">
                                <li class="nav active"><a href="#">Ча-ча-ча</a></li>
                                <li class="nav"><a href="#">Самба</a></li>
                                <li class="nav"><a href="#">Румба</a></li>
                                <li class="nav"><a href="#">Пасодобль</a></li>
                                <li class="nav"><a href="#">Джайв</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container container-padding">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="main-h1 way-dance-h1 after-nav-h1">Преподаватели</h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="teacher-carousel">
                    <div class="teacher-carousel-item">
                        <div class="our-teachers-info">
                            <div class="our-teachers-info-inner golden-border">
                                <div class="teach-info golden-border">
                                    <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="teacher-carousel-item">
                        <div class="our-teachers-info">
                            <div class="our-teachers-info-inner golden-border">
                                <div class="teach-info golden-border">
                                    <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="teacher-carousel-item">
                        <div class="our-teachers-info">
                            <div class="our-teachers-info-inner golden-border">
                                <div class="teach-info golden-border">
                                    <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="teacher-carousel-item">
                        <div class="our-teachers-info">
                            <div class="our-teachers-info-inner golden-border">
                                <div class="teach-info golden-border">
                                    <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="teacher-carousel-item">
                        <div class="our-teachers-info">
                            <div class="our-teachers-info-inner golden-border">
                                <div class="teach-info golden-border">
                                    <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="teacher-carousel-item">
                        <div class="our-teachers-info">
                            <div class="our-teachers-info-inner golden-border">
                                <div class="teach-info golden-border">
                                    <h3>АЛЕКСАНДР ПОЛЯКОВ</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include "footer.php"; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>