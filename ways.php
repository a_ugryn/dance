<!DOCTYPE html>
<html lang="en">
  <?php include "head.php"; ?>
  <body>
  	<?php include "header.php"; ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="main-h1 way-dance-h1">БАЛЬНЫЕ ТАНЦЫ ЛАТИНОАМЕРИКАНСКАЯ ПРОГРАММА</h2>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb text-center">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Направления</a></li>     
                    <li class="active">Бальные танцы латиноамериканская программа</li>
                </ol>
            </div>
        </div>
    </div>
    
    <div class="container way-container">
        <div class="row">
            <div class="col-lg-12 clearfix">
                <a href="#" class="way-item">
                    <img src="img/data/way-1.jpg" alt="">
                    <span class="mask"></span>
                    <p>БАЛЬНЫЕ ТАНЦЫ ЕВРОПЕЙСКАЯ ПРОГРАММА</p>
                </a>
                <a href="#" class="way-item">
                    <img src="img/data/way-1.jpg" alt="">
                    <span class="mask"></span>
                    <p>БАЛЬНЫЕ ТАНЦЫ ЕВРОПЕЙСКАЯ ПРОГРАММА</p>
                </a>
                <a href="#" class="way-item">
                    <img src="img/data/way-1.jpg" alt="">
                    <span class="mask"></span>
                    <p>БАЛЬНЫЕ ТАНЦЫ ЕВРОПЕЙСКАЯ ПРОГРАММА</p>
                </a>
                <a href="#" class="way-item">
                    <img src="img/data/way-1.jpg" alt="">
                    <span class="mask"></span>
                    <p>БАЛЬНЫЕ ТАНЦЫ ЕВРОПЕЙСКАЯ ПРОГРАММА</p>
                </a>
                <a href="#" class="way-item">
                    <img src="img/data/way-1.jpg" alt="">
                    <span class="mask"></span>
                    <p>БАЛЬНЫЕ ТАНЦЫ ЕВРОПЕЙСКАЯ ПРОГРАММА</p>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-ld-12">
                <h1 class="main-h1">уровни обучения</h1>
                <div class="knowledge-container">
                    <div class="knowledge clearfix">
                        <div class="knowledge-item">
                            <div class="knowledge-title color-1">beginner</div>
                            <div class="knowledge-text">Начальный уровень подойдет тем, кто впервые знакомится с бальными танцами или занимался танцами в детстве</div>
                        </div>
                        <div class="knowledge-item">
                            <div class="knowledge-title color-2">continue</div>
                            <div class="knowledge-text">Начальный уровень подойдет тем, кто впервые знакомится с бальными танцами или занимался танцами в детстве</div>
                        </div>
                        <div class="knowledge-item">
                            <div class="knowledge-title color-3">bronze</div>
                            <div class="knowledge-text">Начальный уровень подойдет тем, кто впервые знакомится с бальными танцами или занимался танцами в детстве</div>
                        </div>
                        <div class="knowledge-item">
                            <div class="knowledge-title color-4">silver</div>
                            <div class="knowledge-text">Начальный уровень подойдет тем, кто впервые знакомится с бальными танцами или занимался танцами в детстве</div>
                        </div>
                        <div class="knowledge-item">
                            <div class="knowledge-title color-5">gold</div>
                            <div class="knowledge-text">Начальный уровень подойдет тем, кто впервые знакомится с бальными танцами или занимался танцами в детстве</div>
                        </div>
                    </div>
                </div>
                <span class="text-center">
                    <div class="button">
                        <div class="button-border">
                            <button class="button-inner">Бесплатный урок</button>
                        </div>
                    </div>
                </span>
            </div>
        </div>
    </div>

    <?php include "footer.php"; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>